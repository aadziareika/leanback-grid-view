package com.promwad.leanbackgridview

import android.content.Context
import android.graphics.Color
import android.graphics.Rect
import android.os.Bundle
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.marginRight
import androidx.leanback.widget.HorizontalGridView
import androidx.leanback.widget.VerticalGridView
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import kotlin.math.roundToInt

class MainActivity : AppCompatActivity() {

    private lateinit var mVerticalGridView: RecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        mVerticalGridView = findViewById(R.id.vertical_grid_view)
        mVerticalGridView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        mVerticalGridView.adapter = VerticalGridViewAdapter(this)
    }


    class VerticalGirdViewHolder(view: View) : RecyclerView.ViewHolder(view)


    class VerticalGridViewAdapter(

        private val context: Context

    ) : RecyclerView.Adapter<VerticalGirdViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VerticalGirdViewHolder {
            val layoutId = when (viewType) {
                ITEM_VIEW_TYPE_COMPLEX -> R.layout.layout_vertical_grid_item_complex
                ITEM_VIEW_TYPE_HORIZONTAL_LIST -> R.layout.layout_vertical_glid_item_horizontal_list
                else -> R.layout.layout_vertical_grid_item
            }

            val view = inflate(parent, layoutId)

            setupView(view, viewType)

            return VerticalGirdViewHolder(view)
        }

        override fun onBindViewHolder(holder: VerticalGirdViewHolder, position: Int) {
            when (getItemViewType(position)) {
                ITEM_VIEW_TYPE_TEXT -> {
                    (holder.itemView as TextView).text = DATA_ITEMS[position]
                }
            }
        }

        override fun getItemCount(): Int {
            return DATA_ITEMS.size
        }

        override fun getItemViewType(position: Int): Int {
            return when (position) {
                0 -> ITEM_VIEW_TYPE_COMPLEX
                1 -> ITEM_VIEW_TYPE_HORIZONTAL_LIST
                else -> ITEM_VIEW_TYPE_TEXT
            }
        }

        private fun setupView(view: View, viewType: Int) {
            when (viewType) {
                ITEM_VIEW_TYPE_TEXT -> {
                    (view as TextView).apply {
                        setupFocusability(this)
                        setTextColor(Color.WHITE)
                        gravity = Gravity.CENTER
                    }
                }
                ITEM_VIEW_TYPE_HORIZONTAL_LIST -> {
                    val listView = getHorizontalGridView(view)
                    listView.adapter = HorizontalGridViewAdapter(context)
                    val itemsSpacing = getDimen(R.dimen.sample_horizontal_item_spacing)
                    listView.setItemSpacing(itemsSpacing)
                    val padding = getDimen(R.dimen.sample_padding)

                    listView.setGravity(Gravity.CENTER_VERTICAL)

                    listView.addItemDecoration(
                        MarginalDecoration.createHorizontalDecoration(padding, padding)
                    )

//                    listView.setRowHeight(600)
                    listView.setNumRows(4)

//                    listView.addItemDecoration(
//                        MarginalDecoration.createVerticalDecoration(padding, padding)
//                    )

//                    listView.addItemDecoration(
//                        MarginalItemDecoration(
//                            MarginalItemDecoration.FilterAny(),
//                            MarginalItemDecoration.ItemModifier(Rect(50, 0, 0, 0), MarginalItemDecoration.Margin.TOP::assign)
//                        )
//                    )

                    val titleView = getHorizontalGridViewTitle(view)
                    (titleView.layoutParams as? ViewGroup.MarginLayoutParams)?.let {
                        it.leftMargin = padding
                        it.rightMargin = padding
                    }
                }
            }
        }

        private fun getDimen(dimenId: Int) : Int =
            context.resources.getDimension(dimenId).roundToInt()

        companion object {

            private const val ITEM_VIEW_TYPE_TEXT = 0
            private const val ITEM_VIEW_TYPE_COMPLEX = 1
            private const val ITEM_VIEW_TYPE_HORIZONTAL_LIST = 2

            private val DATA_ITEMS = arrayListOf(
                "Hello world",
                "This is a test",
                "Android TV",
                "Leanback",
                "Apple TV",
                "Amazon",
                "Яндекс.Станция",
                "Wink",
                "Ростелеком",
                "Promwad"
            )
        }
    }

    class HorizontalGridViewHolder(view: View) : RecyclerView.ViewHolder(view)

    class HorizontalGridViewAdapter(
        private val context: Context,
    ) : RecyclerView.Adapter<HorizontalGridViewHolder>() {

        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HorizontalGridViewHolder {
            val view = inflate(parent, R.layout.layout_horizontal_grid_item_screenshot)
            setupFocusability(view)
            return HorizontalGridViewHolder(view)
        }

        override fun onBindViewHolder(holder: HorizontalGridViewHolder, position: Int) {
            val imageView = holder.itemView.findViewById<ImageView>(R.id.sample_image_view)
            Glide.with(context)
                .load(
                    context.resources.getIdentifier(
                        IMAGES_DATA[position], "drawable", context.packageName
                    )
                )
                .override(imageView.width, imageView.height)
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .centerCrop()
                .into(imageView)
        }

        override fun getItemCount(): Int {
            return IMAGES_DATA.size
        }

        companion object {
            private val IMAGES_DATA = arrayListOf(
                "screenshot_4301", "screenshot_4302", "screenshot_4305", "screenshot_4306",
                "screenshot_4307", "screenshot_4308", "screenshot_4309", "screenshot_4310",
                "screenshot_4311", "screenshot_4312", "screenshot_4313", "screenshot_4314",
                "screenshot_4315", "screenshot_4316", "screenshot_4321", "screenshot_4322",
            )
        }
    }

    class MarginalItemDecoration(
        val filter: Filter, val modifier: Modifier
    ) : RecyclerView.ItemDecoration() {

        interface Filter {
            operator fun invoke(dispatcher: FilterDispatcher): Boolean
        }

        interface Modifier {
            operator fun invoke(rect: Rect)
        }

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            if (FilterDispatcher(view, parent, state).dispatch(filter)) modifier(outRect)
        }

        class FilterDispatcher(val view: View, val parent: RecyclerView, val state: RecyclerView.State) {
            fun dispatch(filter: Filter) : Boolean {
                return filter(this)
            }

            fun getItemsCount() = parent.adapter?.itemCount ?: 0

            fun getItemPosition() = parent.getChildAdapterPosition(view)

            fun getLastItemPosition() = getItemsCount() - 1
        }

        class FilterAny : Filter {
            override fun invoke(dispatcher: FilterDispatcher): Boolean {
                return true
            }
        }

        open class FilterPosition(private val mRequiredPosition: Int) : Filter {
            override fun invoke(dispatcher: FilterDispatcher): Boolean {
                return mRequiredPosition == dispatcher.getItemPosition()
            }
        }

        class FilterFirstPosition : FilterPosition(0)

        class FilterLastPosition : Filter {
            override fun invoke(dispatcher: FilterDispatcher): Boolean {
                return with(dispatcher) { getItemPosition() == getLastItemPosition() }
            }
        }

        open class ItemModifier(private val src: Rect, private val modifier: (src: Rect, dst: Rect) -> Unit) : Modifier {
            override fun invoke(rect: Rect) {
                modifier(src, rect)
            }
        }

        class LeftAssignableItemModifier : ItemModifier(Rect(), Margin.LEFT::assign)

        interface MarginModification {
            fun assign(src: Rect, dst: Rect)
            fun concatinate(src: Rect, dst: Rect)
        }

        enum class Margin : MarginModification {
            LEFT {
                override fun assign(src: Rect, dst: Rect) {
                    dst.left = src.left
                }

                override fun concatinate(src: Rect, dst: Rect) {
                    dst.left += src.left
                }
            },

            RIGHT {
                override fun assign(src: Rect, dst: Rect) {
                    dst.right = src.right
                }

                override fun concatinate(src: Rect, dst: Rect) {
                    dst.right += src.right
                }
            },

            TOP {
                override fun assign(src: Rect, dst: Rect) {
                    dst.top = src.top
                }

                override fun concatinate(src: Rect, dst: Rect) {
                    dst.top += src.top
                }
            },

            BOTTOM {
                override fun assign(src: Rect, dst: Rect) {
                    dst.bottom = src.bottom
                }

                override fun concatinate(src: Rect, dst: Rect) {
                    dst.bottom += src.bottom
                }

            }
        }
    }

    class PositionalItemDecoration(private val mModifier: Modifier) : RecyclerView.ItemDecoration() {
        interface Modifier {
            fun onItemMargin(position: Int, count: Int, outRect: Rect)
        }

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            parent.getChildAdapterPosition(view)
                .takeIf { position -> position != RecyclerView.NO_POSITION }
                ?.also { position -> mModifier.onItemMargin(position, getItemsCount(parent), outRect)}
        }

        private fun getItemsCount(parent: RecyclerView) =
            parent.adapter?.itemCount ?: 0

        class FirstItemHorizontalMarginDecoration : Modifier {
            override fun onItemMargin(position: Int, count: Int, outRect: Rect) {
                TODO("Not yet implemented")
            }
        }
    }

    class MarginalDecoration(private val mModifier: Modifier)
        : RecyclerView.ItemDecoration() {

        interface Modifier {
            fun onFirstItemMargin(outRect: Rect)
            fun onLastItemMargin(outRect: Rect)
        }

        class HorizontalModifier(
            private val mLeftMargin: Int,
            private val mRightMargin: Int
        ) : Modifier {

            override fun onFirstItemMargin(outRect: Rect) {
                outRect.left = mLeftMargin
            }

            override fun onLastItemMargin(outRect: Rect) {
                outRect.right = mRightMargin
            }
        }

        class VerticalModifier(
            private val mTopMargin: Int,
            private val mBottomMargin: Int
        ) : Modifier {

            override fun onFirstItemMargin(outRect: Rect) {
                outRect.top = mTopMargin
            }

            override fun onLastItemMargin(outRect: Rect) {
                outRect.bottom = mBottomMargin
            }
        }

        override fun getItemOffsets(
            outRect: Rect,
            view: View,
            parent: RecyclerView,
            state: RecyclerView.State
        ) {
            val position = parent.getChildAdapterPosition(view)
            if (position == RecyclerView.NO_POSITION) return
            if (position == 0) mModifier.onFirstItemMargin(outRect)
            if (position == lastItemIndex(parent)) mModifier.onLastItemMargin(outRect)
        }

        private fun lastItemIndex(parent: RecyclerView) =
            parent.adapter?.let { it.itemCount - 1 } ?: RecyclerView.NO_POSITION

        companion object {
            fun createHorizontalDecoration(leftMargin: Int, rightMargin: Int): RecyclerView.ItemDecoration
                    = MarginalDecoration(HorizontalModifier(leftMargin, rightMargin))

            fun createVerticalDecoration(leftMargin: Int, rightMargin: Int): RecyclerView.ItemDecoration
                    = MarginalDecoration(VerticalModifier(leftMargin, rightMargin))
        }
    }

    companion object {
        private fun setupFocusability(view: View) {
            view.isFocusable = true
            view.isFocusableInTouchMode = true
        }

        private fun inflate(parent: ViewGroup, layoutId: Int): View {
            return LayoutInflater.from(parent.context).inflate(layoutId, parent, false)
        }

        private fun getHorizontalGridView(view: View): HorizontalGridView {
            return view.findViewById(R.id.sample_horizontal_list_item)
        }

        private fun getHorizontalGridViewTitle(view: View): TextView {
            return view.findViewById(R.id.sample_horizontal_list_item_title)
        }
    }
}

fun Rect.concat(src: Rect) {
    left += src.left
    right += src.right
    top += src.top
    bottom += src.bottom
}

