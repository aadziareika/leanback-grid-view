package com.promwad.leanbackgridview

import android.content.Context
import android.util.AttributeSet
import android.util.Log
import android.view.View
import android.widget.FrameLayout

class SampleFrameLayout: FrameLayout {

    constructor(context: Context) : super(context)

    constructor(context: Context, attributeSet: AttributeSet)
            : super(context, attributeSet)

    override fun focusSearch(direction: Int): View? {
        Log.d(TAG, "focusSearch1: direction -> ${focusDirectionToString(direction)}")
        val rv = super.focusSearch(direction)
        Log.d(TAG, "focusSearch1: rv -> $rv")
        return rv
    }

    override fun focusSearch(view: View, direction: Int): View? {
        Log.d(TAG, "focusSearch2: direction -> ${focusDirectionToString(direction)}")
        Log.d(TAG, "focusSearch2: view -> $view")
        val rv = super.focusSearch(view, direction)
        Log.d(TAG, "focusSearch2: rv -> $rv")
        return rv
    }

    companion object {
        private const val TAG = "SampleFrameLayout"

        private fun focusDirectionToString(direction: Int): String {
            return when (direction) {
                FOCUS_UP -> "FOCUS_UP ($direction)"
                FOCUS_DOWN -> "FOCUS_DOWN ($direction)"
                FOCUS_LEFT -> "FOCUS_LEFT ($direction)"
                FOCUS_RIGHT -> "FOCUS_RIGHT ($direction)"
                else -> "<UNKNOWN> ($direction)"
            }
        }
    }
}